const express = require('express');
const app = express();
const mustacheExpress = require('mustache-express');
const bodyParser = require('body-parser');

const pgp = require('pg-promise')();
const session = require('express-session');
const path = require('path');
const userRoutes = require('./routes/users');
const indexRoutes = require('./routes/index');
const checkAuth = require('./utils/checkAuth');
const router = require('./routes/index');



const connectionString = `postgres://postgres:postgresPassword@localhost:5432/newsproject`
const PORT = process.env.PORT || 8000;



const VIEWS_PATH = path.join(__dirname, '/views');

// view engine
app.engine('mustache', mustacheExpress(VIEWS_PATH + '/partials','.mustache'));
app.set('views',VIEWS_PATH);
app.set('view engine', 'mustache');
app.use('/css',express.static('css'));

app.use(session({
  secret: 'monkeydonkey',
  resave: false,
  saveUninitialized: false
}));

router.get('/hello')
app.use(bodyParser.urlencoded({ extended: false }));
db = pgp(connectionString);
app.use((req, res, next) => {
  res.locals.authenticated = req.session.user == null ? false : true;
  next();
});

router.get('/hello', (req, res, next) => {
  res.send('Hello World!')
})


//routes
app.use('/', indexRoutes);
app.use('/users',checkAuth,userRoutes);







// route for articles












app.listen(PORT, () => {
  console.log(`Server has started on ${PORT}`);
});