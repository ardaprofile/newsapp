const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt');


const SALT_ROUNDS = 10


router.get('/', (req, res) => {
  db.any('SELECT articleid, title, body from articles').then((articles) => {
    res.render('index', { articles: articles })
  })
})


// login in route
router.get('/login', (req, res) => {
  res.render('login');
});
router.post('/login', (req, res) => {
 let username = req.body.username;
 let password = req.body.password;
 db.oneOrNone('SELECT userid,username,password FROM users WHERE username = $1', [username])
  .then((user) => {
   if (user) {
    bcrypt.compare(password, user.password, function (error, result) {
     if (result) {
            
      if (req.session) {
       req.session.user = { userId: user.userid, username: user.username }
       // console.log(req.session.user.userId);
              
      }
      res.redirect('/users/articles')
     } else {
      res.render('login', { message: "Invalid username or password" })
     }
    });
   }
   else {
    res.render('login', { message: "Invalid username or password" })
   }
  });
});


// register route
router.post('/register', (req, res) => {
  let username = req.body.username
  let password = req.body.password

  db.oneOrNone(`SELECT userid FROM USERS WHERE username=$1;`, [username]).then(
    (user) => {
      if (user) {
        res.render('register', { message: 'Username Already exists' })
      } else {
        bcrypt.hash(password, SALT_ROUNDS, function (error, hash) {
          if (error == null) {
            db.none(`INSERT INTO users(username,password) VALUES($1,$2)`, [
              username,
              hash,
            ]).then(() => {
              res.send('SUCCESS')
            })
          }
        })
      }
    }
  )
})

router.get('/register', (req, res) => {
  res.render('register')
})

// logout router

router.get('/logout', (req, res, next) => {
  if (req.session) {
    req.session.destroy((error) => {
      if (error) {
        next(error);
      }
      else {
        res.redirect('/login')
      }
    })
  }
});





module.exports = router;