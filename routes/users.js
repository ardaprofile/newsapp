const express = require('express');
const router = express.Router();


// router for getting the articles
router.get('/articles', (req, res) => {
   let userId = req.session.user.userId;
  
  db.any(`SELECT articleid, title, body FROM articles WHERE user_id = $1`, [
    userId,
  ]).then((articles) => {
    res.render('articles', { articles: articles })
  })
})



//route for getting to add article
router.get('/add-article', (req, res) => {
 res.render('add-article')
});

//route for adding article
router.post('/add-article', (req, res) => {
 let title = req.body.title
 let description = req.body.description
 let userId = req.session.user.userId
 //console.log(userId);
 db.none(`INSERT INTO articles(title,body,user_id) VALUES($1,$2,$3)`, [
  title,
  description,
  userId,
 ]).then(() => {
   res.redirect('/users/articles');
 })
});

// route for deleting the article
router.post('/delete-article', (req, res) => {
 let articleId = req.body.articleId
 db.none('DELETE FROM articles WHERE articleid = $1', [articleId]).then(() => {
  res.redirect('/users/articles')
 })
});

// route for updating the article
router.post('/update-article', (req, res) => {
  let title = req.body.title
  let description = req.body.description
  let articleId = req.body.articleId

  db.none('UPDATE articles SET title= $1, body=$2 WHERE articleid =$3', [
    title,
    description,
    articleId,
  ]).then(() => {
    res.redirect('/users/articles')
  })
});

// router for getting the edit article
router.get('/articles/edit/:articleId', (req, res) => {
  let articleId = req.params.articleId;
  db.one('SELECT articleid,title,body FROM articles WHERE articleid=$1', [
    articleId,
  ]).then((article) => {
    res.render('edit-article', article)
  })
})






module.exports = router;