create table  users(userid serial not null primary key, 
 username varchar(10) not null, password varchar(10) not null);



 create table articles(articleid bigserial not null primary key,
 title varchar(50) not null, body text not null,
 datecreated timestamp default current_timestamp,
 dateupdated varchar(50) default current_timestamp,
 user_id serial REFERENCES USERS(userid));

 ALTER TABLE users ALTER COLUMN password TYPE text;

