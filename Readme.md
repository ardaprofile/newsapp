# News App 
## A Fullstack Application for managing the news site.

Technologies Used:
- Node js,
- PostgreSQL,
- pg-promise library,
- Express,
- Mustache Templating Engine
- Body-Parser
- Mustache-Express
- Express-session
- Bcrypt

 ![FrontPage](assets/newsApp1.PNG)
 ![loginPage](assets/newsApp2.PNG)
 ![registerPage](assets/newsApp3.PNG)
 ![UserPage](assets/newsApp4.PNG)
 ![AddarticlePage](assets/newsApp5.PNG)




